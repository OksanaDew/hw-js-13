let wrapper = document.getElementById('wrapper');
let nav = document.getElementById('header-navigation');
nav.style.backgroundColor = 'rgb(53, 68, 79)';


    let btn = document.createElement('button');
btn.innerHTML = 'Сменить тему';
wrapper.before(btn);
btn.addEventListener('click', changeStyle);

function changeStyle() {
    if(nav.style.backgroundColor === 'rgb(53, 68, 79)') {
        nav.style.backgroundColor = 'red';
        localStorage.setItem('nav.style.backgroundColor', 'red');
    }else {
        nav.style.backgroundColor = 'rgb(53, 68, 79)';
        localStorage.setItem('nav.style.backgroundColor', 'rgb(53, 68, 79)');
    }
}